package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/opentracing/opentracing-go/ext"
	"github.com/prometheus/common/log"

	opentracing "github.com/opentracing/opentracing-go"
)

type helloWorldHandler struct {
}

func (h *helloWorldHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	url := r.URL
	name := r.FormValue("name")

	span := opentracing.StartSpan("sayHello", ext.SpanKindRPCServer)
	defer span.Finish()
	span.SetTag("to-who", name)
	span.SetTag("url", url)

	ctx := context.Background()
	ctx = opentracing.ContextWithSpan(ctx, span)

	response := formatHello(ctx, name)
	printHello(ctx, w, response)
}

func printHello(ctx context.Context, w http.ResponseWriter, r string) {
	span, _ := opentracing.StartSpanFromContext(ctx, "printHello")
	defer span.Finish()
	w.Write([]byte(r))
	span.LogKV("event", "print")
}

func formatHello(ctx context.Context, name string) string {
	span, _ := opentracing.StartSpanFromContext(ctx, "format")
	defer span.Finish()

	v := url.Values{}
	v.Set("name", name)

	url := "http://localhost:8889/format?" + v.Encode()
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Errorln(err)
		return ""
	}
	ext.SpanKindRPCClient.Set(span)
	ext.HTTPUrl.Set(span, url)
	ext.HTTPMethod.Set(span, "GET")
	span.Tracer().Inject(span.Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(req.Header))

	resp, err := doRequest(req)
	if err != nil {
		log.Errorln(err)
		return ""
	}
	return string(resp)
}

func doRequest(req *http.Request) ([]byte, error) {
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("StatusCode: %d, Body: %s", resp.StatusCode, body)
	}

	return body, nil
}
