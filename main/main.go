package main

import (
	"jaegerdemo/tracing"
	"log"
	"net/http"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {

	tracer, closer := tracing.SetupTracing("jaegerdemo", "demomain")
	opentracing.InitGlobalTracer(tracer)

	defer closer.Close()
	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/sayhello", &helloWorldHandler{})
	log.Fatal(http.ListenAndServe("0.0.0.0:8888", nil))
}
