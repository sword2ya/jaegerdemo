package metricsfactory

import (
	"log"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	jmet "github.com/uber/jaeger-lib/metrics"
)

type rpcMetrixFactory struct {
	nameSpace string
	subSystem string
}

type rpcCounter struct {
	counter prometheus.Counter
}

func (r *rpcCounter) Inc(v int64) {
	r.counter.Add((float64)(v))
}

type rpcTimer struct {
	histogram prometheus.Histogram
}

func (r *rpcTimer) Record(v time.Duration) {
	r.histogram.Observe(float64(v) / 1000.0)
}

type rpcTimerBySummary struct {
	summary prometheus.Summary
}

func (r *rpcTimerBySummary) Record(v time.Duration) {
	r.summary.Observe(float64(v) / 1000.0)
}

type rpcGauge struct {
	gauge prometheus.Gauge
}

func (r *rpcGauge) Update(v int64) {
	r.gauge.Set(float64(v))
}

/*
func (f *rpcMetrixFactory) Timer(name string, tags map[string]string) jmet.Timer {
	promeHistogram := prometheus.NewHistogram(prometheus.HistogramOpts{
		Namespace:   f.nameSpace,
		Subsystem:   f.subSystem,
		Name:        name,
		ConstLabels: tags,
		Help:        "Why need help string？",
	})
	prometheus.MustRegister(promeHistogram)

	log.Println("new timer:", name)

	return &rpcTimer{
		histogram: promeHistogram,
	}
}
*/

func (f *rpcMetrixFactory) Timer(name string, tags map[string]string) jmet.Timer {
	promeSummary := prometheus.NewSummary(prometheus.SummaryOpts{
		Namespace:   f.nameSpace,
		Subsystem:   f.subSystem,
		Name:        name,
		ConstLabels: tags,
		Help:        "Why need help string？",
		Objectives:  map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
	})
	prometheus.MustRegister(promeSummary)

	log.Println("new timer:", name)

	return &rpcTimerBySummary{
		summary: promeSummary,
	}
}

func (f *rpcMetrixFactory) Counter(name string, tags map[string]string) jmet.Counter {
	promeCounter := prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace:   f.nameSpace,
			Subsystem:   f.subSystem,
			Name:        name,
			ConstLabels: tags,
			Help:        "Why need help string?",
		})
	prometheus.MustRegister(promeCounter)
	log.Println("new counter:", name)

	return &rpcCounter{
		counter: promeCounter,
	}
}

func (f *rpcMetrixFactory) Gauge(name string, tags map[string]string) jmet.Gauge {
	promeGauge := prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace:   f.nameSpace,
			Subsystem:   f.subSystem,
			Name:        name,
			ConstLabels: tags,
			Help:        "Why need help string?",
		})
	log.Println("new gauge:", name)
	prometheus.MustRegister(promeGauge)
	return &rpcGauge{
		gauge: promeGauge,
	}
}

func (f *rpcMetrixFactory) Namespace(name string, tags map[string]string) jmet.Factory {
	log.Println("create new factory:", name)
	return &rpcMetrixFactory{
		nameSpace: f.nameSpace,
		subSystem: f.subSystem,
	}
}

// CreateMetrixFactory create a metrix factory that adapter to jaeger metrix factory
func CreateMetrixFactory(nameSpace string, subSystem string) jmet.Factory {
	return &rpcMetrixFactory{
		nameSpace: nameSpace,
		subSystem: subSystem,
	}
}
