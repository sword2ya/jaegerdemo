package tracing

import (
	"fmt"
	"io"
	"jaegerdemo/tracing/metricsfactory"

	opentracing "github.com/opentracing/opentracing-go"
	jaeger "github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
)

// SetupTracing init opentracing with jaeger
func SetupTracing(nameSpace string, serviceName string) (opentracing.Tracer, io.Closer) {
	cfg := &config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans: true,
		},
		RPCMetrics: true,
	}
	f := metricsfactory.CreateMetrixFactory(nameSpace, serviceName)
	tracer, closer, err := cfg.New(serviceName, config.Logger(jaeger.StdLogger), config.Metrics(f))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	return tracer, closer
}
