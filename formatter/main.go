package main

import (
	"fmt"
	"jaegerdemo/tracing"
	"log"
	"net/http"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

func main() {
	tracer, closer := tracing.SetupTracing("jaegerdemo", "demoformater")
	defer closer.Close()
	opentracing.InitGlobalTracer(tracer)

	http.HandleFunc("/format", func(w http.ResponseWriter, r *http.Request) {
		spanCtx, _ := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
		span := tracer.StartSpan("format", ext.RPCServerOption(spanCtx))
		defer span.Finish()

		helloTo := r.FormValue("name")
		helloStr := fmt.Sprintf("Hello, %s!", helloTo)
		w.Write([]byte(helloStr))
	})

	log.Fatal(http.ListenAndServe("0.0.0.0:8889", nil))
}
