# Jaegerdemo

## 简介

jaegerdemo用于阐述如何将jaeger和prometheus搭配使用，用于观测每个span的性能情况。该文档会介绍jaegerdemo是如何工作的，同时也会讲述在实现过程中遇到的一些问题和解决方法，并且会对prometheus中的一些基本概念做简单的描述。


## Prometheus是什么

Prometheus是一个统计系统，用于统计程序中产生的一系列数据，或者统计程序中的某些过程的性能情况。
Prometheus有4种基础统计类型，分别为 Counter, Gauge, Histogram和Summary。
- Counter用于递增计数统计，数值只能增加不能减少
- Gauge用于统计动态变化的数据，数值可以增加和减少
- Histogram为直方图统计，主要用于请求执行时间。Histogram将目标统计数据按照区间排列，并且在客户端界面给出落在每个区间内的命中数。 
- Summary类似Histogram，也主要用于统计请求执行时间等性能数据。和Histogram的区别是，Summary不将目标数据分区间，而是给出一个平滑的统计曲线。Summary更适合用于统计中位数、95%，99%等分位数数据，Summary也可以自定义增加其他的百分分位数观测点


## Jaeger是什么

Jaeger是opentracing的一个实现，用于跟踪代码的调用链。

### 怎么统计Jaeger中Span的性能情况
Jaeger-client-go包中有一个rpcMetrix的概念，用于将span的数据通过http接口展示给其它的统计系统（如Prometheus）。rpcMetrix只是提供了一个抽象接口，具体怎么统计需要我们自己去实现。先来看一下rpcMetrix提供的抽象接口的形式：  
```go
// Factory creates new metrics
type Factory interface {
	Counter(name string, tags map[string]string) Counter
	Timer(name string, tags map[string]string) Timer
	Gauge(name string, tags map[string]string) Gauge

	// Namespace returns a nested metrics factory.
	Namespace(name string, tags map[string]string) Factory
}
中位数、95%，99%分位数是很重要的性能指标。 
// Counter tracks the number of times an event has occurred
type Counter interface {
	// Inc adds the given value to the counter.
	Inc(int64)
}

// Gauge returns instantaneous measurements of something as an int64 value
type Gauge interface {
	// Update the gauge to the value passed in.
	Update(int64)
}

// Timer accumulates observations about how long some operation took,
// and also maintains a historgam of percentiles.
type Timer interface {
	// Records the time passed in.
	Record(time.Duration)
}
```
我们需要提供的就是这样的一个Factory，用于生成Counter、Timer、Gauge，或者创建另外一个Factory。可以看出来，Counter和Gauge可以分别用Prometheus的Counter和Gauge实现。Timer则需要使用Histogram或者Summary实现，根据前面所说的Histogram与Summary的区别，demo中我们使用Summary实现这个Timer，因为中位数、95%，99%分位数是很重要的性能指标。  
那我们创建的Factory是怎么和jaeger关联起来的呢？再看下面的代码：  


```go
// Options control behavior of the client.
type Options struct {
	metrics             metrics.Factory
	...
}

// Metrics creates an Option that initializes Metrics in the tracer,
// which is used to emit statistics about spans.
func Metrics(factory metrics.Factory) Option {
	return func(c *Options) {
		c.metrics = factory
	}
}

func (c Configuration) New(
	serviceName string,
	options ...Option,
) (opentracing.Tracer, io.Closer, error) {
    ...
	if c.RPCMetrics {
		Observer(
			rpcmetrics.NewObserver(
				opts.metrics.Namespace("jaeger-rpc", map[string]string{"component": "jaeger"}),
				rpcmetrics.DefaultNameNormalizer,
			),
		)(&opts) // adds to c.observers
	}
	...
}

```
函数New是jaeger用于创建tracer的接口，它会判断Configuration中的RPCMetrics是否为true，如果是true就会创建一个Observer用于观察span的生命周期等性能指标，并且将数据通过http暴露给其他监控系统（如Promethues）。
Metrics接收一个Factory作为参数并且返回一个可以传入New中的Option，我们可以把自己的Factory作为参数使用Metrics函数生成Option。

## demo的代码结构
在metrixadapter.go中，我实现了一个rpcMetrixFactory，它满足Jaeger的metrics.Factory接口。
在main.go中，setupJaeger创建了rpcMetrixFactory的实例，并且传入Jaeger的初始化函数中。
helloworld.go中实现了一个简单的http接口，并且在该接口的实现中植入了opentracing跟踪。

## 如何使用
1. 编译并且运行jaegerdemo
2. 启动jaeger服务器  
```bash  
docker run -d -e COLLECTOR_ZIPKIN_HTTP_PORT=9411 -p5775:5775/udp -p6831:6831/udp -p6832:6832/udp -p5778:5778 -p16686:16686 -p14268:14268 -p9411:9411 jaegertracing/all-in-one:latest
```
3. 配置并启动promethues
4. 访问 localhost:8888/sayhello产生一些调用数据
5. 通过promethues的后台观测性能 localhost:9090


## 遇到的问题

### Namespace名字问题
在Namespace函数中，我们返回的Factory不能使用参数传入的name作为namespace，因为jaeger会使用 "jaeger-rpc"这种名字来调用该函数，而promethues不支持横线"-"这种名称。
```go
func (f *rpcMetrixFactory) Namespace(name string, tags map[string]string) jmet.Factory {
	log.Println("create new factory:", name)
	return &rpcMetrixFactory{
		nameSpace: f.nameSpace,
		subSystem: f.subSystem,
	}
}
```

### span的kind
使用opentracing.StartSpan函数，只有传入ext.SpanKindRPCServer才能够被jaeger使用rpcmetrics暴露该span的数据，否则不会暴露。   
```go 
span := opentracing.StartSpan("sayHello", ext.SpanKindRPCServer)
```

### vendor
1. jaeger-client-go包有自己依赖的第三方库版本，所以应该按照官方介绍来安装jaeger-client-go包，否则编译会失败   
```bash 
go get -u github.com/uber/jaeger-client-go/
cd $GOPATH/src/github.com/uber/jaeger-client-go/
git submodule update --init --recursive
make install
```

2. jaeger-client-go安装完之后，它的目录下面会有一个vendor（详情请参考go官方文档）目录，包括了一些第三方库。但我们自己使用的opentracing-go和jaeger-lib等包会和这些第三方库发生冲突。所以应该将jaeger-client-go的vendor目录内容移动到demo中的vendor目录下。
